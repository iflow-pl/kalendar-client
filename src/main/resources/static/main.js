(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".active {\r\n  background-color: green;\r\n  color: white;\r\n}\r\n\r\n.pedding {\r\n  background-color: lightgrey;\r\n}\r\n\r\n.complete {\r\n  background-color: cadetblue;\r\n  color: white;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWTtBQUNkOztBQUVBO0VBQ0UsMkJBQTJCO0FBQzdCOztBQUVBO0VBQ0UsMkJBQTJCO0VBQzNCLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2ZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ucGVkZGluZyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xyXG59XHJcblxyXG4uY29tcGxldGUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGNhZGV0Ymx1ZTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"sticky-top\">\n<div class=\"navbar navbar-light bg-light\">\n  iFlow Appointment System  <div [hidden]=\"!showTikTok\" class=\"countDown blink\">{{tiktok}}</div>\n</div>\n\n<div class=\"container-fluid \" >\n  <div class=\"row\">\n    <div class=\"card col-3 p-0 m-0\"   >\n      <div   [className]=\"activeLinks.branches.status==0 ? 'pedding' : activeLinks.branches.status==1 ? 'active' : 'complete'\"  >\n      <div class=\"card-header flex-fill d-none d-md-block\">\n          {{messages.common.stage.branch.title}}\n      </div>\n        <div  class=\"text-center card-header flex-fill d-block d-sm-block d-md-none\">\n          <img src=\"assets/icons/calendar-fill.svg\" alt=\"\" width=\"32\" height=\"32\">\n        </div>\n      </div>\n    </div>\n    <div class=\"card col-3 p-0 m-0\" >\n      <div [className]=\" activeLinks.customers.status==0 ? 'pedding' : activeLinks.customers.status==1 ? 'active' : 'complete'\">\n        <div class=\"card-header flex-fill d-none d-md-block\">\n          {{messages.common.stage.customer.title}}\n        </div>\n        <div  class=\"text-center card-header flex-fill d-block d-sm-block d-md-none\">\n          <img src=\"assets/icons/credit-card.svg\" alt=\"\" width=\"32\" height=\"32\">\n        </div>\n      </div>\n    </div>\n    <div class=\"card col-3 p-0 m-0\"  >\n      <div [className]=\" activeLinks.verification.status==0 ? 'pedding' : activeLinks.verification.status==1 ? 'active' : 'complete'\">\n        <div class=\"card-header flex-fill d-none d-md-block\">\n          {{messages.common.stage.verification.title}}\n        </div>\n        <div  class=\"text-center card-header flex-fill d-block d-sm-block d-md-none\">\n          <img src=\"assets/icons/lock-fill.svg\" alt=\"\" width=\"32\" height=\"32\">\n        </div>\n      </div>\n    </div>\n    <div class=\"card col-3  p-0 m-0\">\n      <div [className]=\" activeLinks.confirmation.status==0 ? 'pedding' : activeLinks.confirmation.status==1 ? 'active' : 'complete'\">\n        <div class=\"card-header flex-fill d-none d-md-block\">\n          {{messages.common.stage.confirmation.title}}\n        </div>\n        <div  class=\"text-center card-header flex-fill d-block d-sm-block d-md-none\">\n          <img src=\"assets/icons/flag-fill.svg?fill='%23000'\" alt=\"\" width=\"32\" height=\"32\" fill='%23000'>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n<div class=\"\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/appstate.service */ "./src/app/services/appstate.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/ui-configration-service.service */ "./src/app/services/ui-configration-service.service.ts");






var AppComponent = /** @class */ (function () {
    function AppComponent(appStateService, route) {
        var _this = this;
        this.appStateService = appStateService;
        this.route = route;
        this.branchInfo = { name: '' };
        this.serviceInfo = { name: '' };
        this.dateInfo = undefined;
        this.timeslotInfo = undefined;
        this.tiktok = '';
        this.showTikTok = true;
        // status : 0 - pedding, 1 - active, 2 - complete
        this.activeLinks = { branches: { status: 1 }, customers: { status: 0 }, verification: { status: 0 }, confirmation: { status: 0 } };
        this.activeIndex = 1;
        this.title = 'calendar-client-frontend';
        this.branchStateObserver = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subscription"]();
        this.messages = _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_5__["UiConfigrationServiceService"].myLanguage;
        this.appStateService.applicationStagesObserver.subscribe(function (stage) {
            if (stage.id === 0) {
                _this.activeLinks.branches.status = 1;
                _this.activeLinks.customers.status = 0;
                _this.activeLinks.verification.status = 0;
                _this.activeLinks.confirmation.status = 0;
            }
            else if (stage.id === 1) {
                _this.activeLinks.branches.status = 2;
                _this.activeLinks.customers.status = 1;
                _this.activeLinks.verification.status = 0;
                _this.activeLinks.confirmation.status = 0;
            }
            else if (stage.id === 2) {
                _this.activeLinks.branches.status = 2;
                _this.activeLinks.customers.status = 2;
                _this.activeLinks.verification.status = 1;
                _this.activeLinks.confirmation.status = 0;
            }
            else if (stage.id === 3) {
                _this.activeLinks.branches.status = 2;
                _this.activeLinks.customers.status = 2;
                _this.activeLinks.verification.status = 2;
                _this.activeLinks.confirmation.status = 1;
            }
        });
        this.branchStateObserver = this.appStateService.branchStateObserver.subscribe(function (branchInfo) {
        });
        this.appStateService.appointmentReservationObserver.subscribe(function (reservationInfo) {
            console.log("appointmentReservationObserver", reservationInfo);
            _this.startTimer(10);
            _this.openTab("/customer");
        });
        this.appStateService.finishedBookingObserver.subscribe(function (finish) {
            _this.appStateService.resetAllState();
            _this.route.navigateByUrl("/branches");
        });
        this.appStateService.confirmationStateObserver.subscribe(function (confirm) {
            _this.successfulBook();
        });
    }
    AppComponent.prototype.ngOnDestroy = function () {
        this.branchStateObserver.unsubscribe();
    };
    AppComponent.prototype.openTab = function (path) {
        this.route.navigateByUrl(path);
    };
    AppComponent.prototype.sessionOver = function () {
        clearInterval(this.sessionInterval);
        this.appStateService.resetAllState();
        this.route.navigateByUrl("/branches");
    };
    AppComponent.prototype.successfulBook = function () {
        clearInterval(this.sessionInterval);
        this.showTikTok = false;
    };
    AppComponent.prototype.startTimer = function (display) {
        var _this = this;
        var timer = display * 60;
        var minutes;
        var seconds;
        this.sessionInterval = setInterval(function () {
            minutes = Math.floor(timer / 60);
            seconds = Math.floor(timer % 60);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            _this.tiktok = minutes + ':' + seconds;
            --timer;
            if (timer < 0) {
                _this.showTikTok = false;
                _this.sessionOver();
            }
        }, 1000);
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_appstate_service__WEBPACK_IMPORTED_MODULE_2__["AppstateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: initConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initConfig", function() { return initConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _component_branches_branches_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/branches/branches.component */ "./src/app/component/branches/branches.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _routers_router_router_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./routers/router/router.module */ "./src/app/routers/router/router.module.ts");
/* harmony import */ var _component_customers_customers_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./component/customers/customers.component */ "./src/app/component/customers/customers.component.ts");
/* harmony import */ var _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/ui-configration-service.service */ "./src/app/services/ui-configration-service.service.ts");
/* harmony import */ var _component_conformation_conformation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./component/conformation/conformation.component */ "./src/app/component/conformation/conformation.component.ts");
/* harmony import */ var _component_verification_verification_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./component/verification/verification.component */ "./src/app/component/verification/verification.component.ts");












function initConfig(uiConfigrationServiceService) {
    return function () { return uiConfigrationServiceService.fetchUiConfigration(); };
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _component_branches_branches_component__WEBPACK_IMPORTED_MODULE_5__["BranchesComponent"],
                _component_customers_customers_component__WEBPACK_IMPORTED_MODULE_8__["CustomersComponent"],
                _component_conformation_conformation_component__WEBPACK_IMPORTED_MODULE_10__["ConformationComponent"],
                _component_verification_verification_component__WEBPACK_IMPORTED_MODULE_11__["VerificationComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _routers_router_router_module__WEBPACK_IMPORTED_MODULE_7__["RoutersModule"]
            ],
            entryComponents: [],
            providers: [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_9__["UiConfigrationServiceService"], { provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["APP_INITIALIZER"], useFactory: initConfig, deps: [_services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_9__["UiConfigrationServiceService"]], multi: true }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/component/branches/branches.component.css":
/*!***********************************************************!*\
  !*** ./src/app/component/branches/branches.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9icmFuY2hlcy9icmFuY2hlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/component/branches/branches.component.html":
/*!************************************************************!*\
  !*** ./src/app/component/branches/branches.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"container-fluid\" style=\"margin-top: 20px\">\n    <div class=\"form-group\">\n    <label for=\"branches\">{{myLanguage.component.html.branches.label.selectBranch}}</label>\n      <select id=\"branches\" [(ngModel)]=\"selectedBranchPublicId\" (change)=\"onBranchSelectChange($event)\" [disabled]=\"lockBranchOnLoad\" class=\"form-control\">\n          <option value=\"-1\" disabled >{{myLanguage.component.html.branches.value.selectDefault}}</option>\n          <option *ngFor=\"let branch of branches\" [value]=\"branch.publicId\">{{branch.name}}</option>\n      </select>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"services\">{{myLanguage.component.html.branches.label.selectService}}</label>\n      <select id=\"services\" [(ngModel)]=\"selectedServicePublicId\" (change)=\"onServiceSelectChange($event)\" [disabled]=\"lockServiceOnLoad\" class=\"form-control\">\n        <option value=\"-1\" disabled >{{myLanguage.component.html.branches.value.selectDefault}}</option>\n        <option *ngFor=\"let service of services\" [value]=\"service.publicId\">{{service.name}}</option>\n      </select>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"dates\">{{myLanguage.component.html.branches.label.selectDate}}</label>\n      <select id=\"dates\" [(ngModel)]=\"selectedDate\" (change)=\"onDateSelectChange($event)\" [disabled]=\"lockDateOnLoad\" class=\"form-control\">\n        <option value=\"-1\" disabled >{{myLanguage.component.html.branches.value.selectDefault}}</option>\n        <option *ngFor=\"let date of dates\" [value]=\"date\">{{date | date:'dd-MM-yyyy'}}</option>\n      </select>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"timeslots\">{{myLanguage.component.html.branches.label.selectTimeSlot}}</label>\n      <select id=\"timeslots\" [(ngModel)]=\"selectedTimeSlots\" (change)=\"onTimeSelectChange($event)\" [disabled]=\"lockTimeSlotOnLoad\" class=\"form-control\">\n        <option value=\"-1\" disabled >{{myLanguage.component.html.branches.value.selectDefault}}</option>\n        <option *ngFor=\"let timeslot of timeslots\" [value]=\"timeslot\">{{timeslot}}</option>\n      </select>\n    </div>\n\n  </div>\n  <div class=\"container-fluid align-items-center\" style=\"margin-top: 20px\">\n  <button class=\"btn btn-success btn-block\" (click)=\"onNext()\" [disabled]=\"!appoinmentValid\">{{myLanguage.component.html.branches.label.button.next}}</button>\n</div>\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/component/branches/branches.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/component/branches/branches.component.ts ***!
  \**********************************************************/
/*! exports provided: BranchesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchesComponent", function() { return BranchesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_calendar_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/calendar.service */ "./src/app/services/calendar.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/appstate.service */ "./src/app/services/appstate.service.ts");
/* harmony import */ var _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/ui-configration-service.service */ "./src/app/services/ui-configration-service.service.ts");






var BranchesComponent = /** @class */ (function () {
    function BranchesComponent(calendarService, appStateService, route, router) {
        this.calendarService = calendarService;
        this.appStateService = appStateService;
        this.route = route;
        this.router = router;
        this.myLanguage = _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_5__["UiConfigrationServiceService"].myLanguage;
        this.branches = [];
        this.services = [];
        this.dates = [];
        this.timeslots = [];
        this.selectedBranchPublicId = '-1';
        this.selectedServicePublicId = '-1';
        this.lockBranchOnLoad = false;
        this.lockServiceOnLoad = false;
        this.lockDateOnLoad = false;
        this.lockTimeSlotOnLoad = false;
        this.selectedBranch = {};
        this.selectedService = {};
        this.selectedDate = '-1';
        this.selectedTimeSlots = '-1';
        this.providedParameter = {
            branch: { value: '' },
            service: { value: '' },
            date: { value: '' },
            timeslot: { value: '' }
        };
        this.appoinmentValid = false;
        this.branchState = { branch: {}, service: {}, date: '', time: '' };
        var preappattfromext = parent.document.getElementById('preappattfromext');
        if (preappattfromext) {
            if (preappattfromext.dataset.hasOwnProperty('bidfe')) {
                this.providedParameter.branch.value = preappattfromext.dataset.bidfe;
                this.selectedBranchPublicId = this.providedParameter.branch.value;
                this.lockBranchOnLoad = true;
            }
            if (preappattfromext.dataset.hasOwnProperty('sidfe')) {
                this.providedParameter.service.value = preappattfromext.dataset.sidfe;
                this.selectedServicePublicId = this.providedParameter.service.value;
                this.lockServiceOnLoad = true;
            }
            if (preappattfromext.dataset.hasOwnProperty('btfe')) {
                this.providedParameter.date.value = preappattfromext.dataset.btfe;
                this.selectedDate = this.providedParameter.date.value;
                this.lockDateOnLoad = true;
            }
            if (preappattfromext.dataset.hasOwnProperty('tsfe')) {
                this.providedParameter.timeslot.value = preappattfromext.dataset.tsfe;
                this.selectedTimeSlots = this.providedParameter.timeslot.value;
                this.lockTimeSlotOnLoad = true;
            }
            console.log(this.providedParameter);
        }
        this.fetchBranches().then(function (d) {
        });
    }
    BranchesComponent.prototype.ngOnInit = function () {
    };
    BranchesComponent.prototype.fetchBranches = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var branches, _a, e_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, this.calendarService.getBranches()];
                    case 1:
                        branches = _b.sent();
                        this.branches = branches;
                        console.log(this.branches);
                        if (!(this.selectedBranchPublicId != '-1')) return [3 /*break*/, 4];
                        _a = this;
                        return [4 /*yield*/, this.findBranchByBranchPublicId(this.selectedBranchPublicId)];
                    case 2:
                        _a.selectedBranch = _b.sent();
                        this.storeSelectedBranchInfo(this.selectedBranch);
                        return [4 /*yield*/, this.fetchServices()];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        console.log(this.selectedBranch);
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _b.sent();
                        console.log(e_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    BranchesComponent.prototype.fetchServices = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var services, _a, e_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.calendarService.getServicesByBranchPublicId(this.selectedBranchPublicId)];
                    case 1:
                        services = _b.sent();
                        this.services = services;
                        console.log("Services : ", this.services);
                        if (!(this.selectedServicePublicId != '-1')) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.findServiceByServicePublicId(this.selectedServicePublicId)];
                    case 2:
                        _a.selectedService = _b.sent();
                        this.storeSelectedServiceInfo(this.selectedService);
                        this.fetchDates();
                        _b.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_2 = _b.sent();
                        console.log(e_2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    BranchesComponent.prototype.fetchDates = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var dates, e_3;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.calendarService.getDatesByBranchPublicIdAndServicePublicId(this.selectedBranchPublicId, this.selectedServicePublicId)];
                    case 1:
                        dates = _a.sent();
                        this.dates = dates;
                        console.log("Dates : ", this.dates);
                        if (this.selectedDate != '-1') {
                            this.storeSelectedDateInfo(this.selectedDate);
                            this.fetchTimeSlots();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        console.log(e_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BranchesComponent.prototype.fetchTimeSlots = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var tempSelectedDate, yyyyMMddString, timeSlots, e_4;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        tempSelectedDate = new Date(this.selectedDate);
                        yyyyMMddString = tempSelectedDate.getFullYear() + '-' + (tempSelectedDate.getMonth() + 1) + '-' + tempSelectedDate.getDate();
                        console.log(this.selectedBranchPublicId, this.selectedServicePublicId, yyyyMMddString);
                        return [4 /*yield*/, this.calendarService.getTimeslotsByBranchPublicIdAndServicePublicIdAndDate(this.selectedBranchPublicId, this.selectedServicePublicId, yyyyMMddString)];
                    case 1:
                        timeSlots = _a.sent();
                        this.timeslots = timeSlots;
                        console.log("Timeslots : ", this.timeslots);
                        if (this.selectedTimeSlots != '-1') {
                            // To do
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_4 = _a.sent();
                        console.log(e_4);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BranchesComponent.prototype.findBranchByBranchPublicId = function (branchPublicId) {
        return this.branches.find(function (o, i) {
            if (o.publicId == branchPublicId) {
                return true;
            }
        });
    };
    BranchesComponent.prototype.findServiceByServicePublicId = function (servicePublicId) {
        return this.services.find(function (o, i) {
            if (o.publicId == servicePublicId) {
                return true;
            }
        });
    };
    BranchesComponent.prototype.onBranchSelectChange = function (event) {
        this.selectedBranch = this.findBranchByBranchPublicId(event.target.value);
        // Update dependent values
        this.clearService();
        this.clearDate();
        this.clearTime();
        this.fetchServices();
        this.storeSelectedBranchInfo(this.selectedBranch);
        console.log('Selected Branch', this.selectedBranch);
    };
    BranchesComponent.prototype.onServiceSelectChange = function (event) {
        this.selectedService = this.findServiceByServicePublicId(event.target.value);
        // Update dependent values
        this.clearDate();
        this.clearTime();
        this.fetchDates();
        this.storeSelectedServiceInfo(this.selectedService);
        console.log('Selected Service', this.selectedService);
    };
    BranchesComponent.prototype.onDateSelectChange = function (event) {
        this.clearTime();
        this.fetchTimeSlots();
        this.storeSelectedDateInfo(this.selectedDate);
        console.log('Selected Date', this.selectedDate);
    };
    BranchesComponent.prototype.onTimeSelectChange = function (event) {
        this.storeSelectedTimeSlotInfo(this.selectedTimeSlots);
        console.log('Selected Timeslot', this.selectedTimeSlots);
    };
    BranchesComponent.prototype.branchStateChange = function (event) {
        console.log("branchStateChange", event);
    };
    BranchesComponent.prototype.onNext = function () {
        var _this = this;
        this.appStateService.setBranchState(this.branchState);
        this.calendarService.reserveAppointment(this.branchState).then(function (data) {
            _this.appStateService.setAppointmentReservationObserver(data);
        });
    };
    BranchesComponent.prototype.storeSelectedBranchInfo = function (branchInfo) {
        this.branchState.branch = branchInfo;
        this.isAppointmentValid();
    };
    BranchesComponent.prototype.storeSelectedServiceInfo = function (serviceInfo) {
        this.branchState.service = serviceInfo;
        this.isAppointmentValid();
    };
    BranchesComponent.prototype.storeSelectedDateInfo = function (dateInfo) {
        this.branchState.date = dateInfo;
        this.isAppointmentValid();
    };
    BranchesComponent.prototype.storeSelectedTimeSlotInfo = function (timeSlotInfo) {
        this.branchState.time = timeSlotInfo;
        this.isAppointmentValid();
    };
    BranchesComponent.prototype.isAppointmentValid = function () {
        console.log(this.branchState);
        if (this.branchState.branch.hasOwnProperty("publicId") && this.branchState.service.hasOwnProperty("publicId") && this.branchState.date && this.branchState.time) {
            this.appoinmentValid = true;
        }
        else {
            this.appoinmentValid = false;
        }
    };
    BranchesComponent.prototype.clearBranch = function () {
    };
    BranchesComponent.prototype.clearService = function () {
        this.selectedServicePublicId = '-1';
        this.branchState.service = {};
    };
    BranchesComponent.prototype.clearDate = function () {
        this.selectedDate = '-1';
        this.dates = [];
        this.branchState.date = '';
    };
    BranchesComponent.prototype.clearTime = function () {
        this.selectedTimeSlots = '-1';
        this.timeslots = [];
        this.branchState.time = '';
    };
    BranchesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-branches',
            template: __webpack_require__(/*! ./branches.component.html */ "./src/app/component/branches/branches.component.html"),
            styles: [__webpack_require__(/*! ./branches.component.css */ "./src/app/component/branches/branches.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_calendar_service__WEBPACK_IMPORTED_MODULE_2__["CalendarService"], _services_appstate_service__WEBPACK_IMPORTED_MODULE_4__["AppstateService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], BranchesComponent);
    return BranchesComponent;
}());



/***/ }),

/***/ "./src/app/component/conformation/conformation.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/component/conformation/conformation.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9jb25mb3JtYXRpb24vY29uZm9ybWF0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/component/conformation/conformation.component.html":
/*!********************************************************************!*\
  !*** ./src/app/component/conformation/conformation.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center container-fluid\" style=\"overflow-wrap: break-word;\">\n\n  <h1>Congratulations</h1>\n\n  Your appointment Id <br> <br>\n  <p> {{confirmationState.publicId}} </p>\n\n  <button class=\"btn btn-success btn-block\" (click)=\"closeApp()\"> Close </button>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/component/conformation/conformation.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/component/conformation/conformation.component.ts ***!
  \******************************************************************/
/*! exports provided: ConformationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConformationComponent", function() { return ConformationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/appstate.service */ "./src/app/services/appstate.service.ts");



var ConformationComponent = /** @class */ (function () {
    function ConformationComponent(appStateService) {
        this.appStateService = appStateService;
        this.confirmationState = appStateService.getConfirmationState();
    }
    ConformationComponent.prototype.ngOnInit = function () {
    };
    ConformationComponent.prototype.closeApp = function () {
        localStorage.setItem('currentStage', '0');
        this.appStateService.setFinishedBookingObserver();
    };
    ConformationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-conformation',
            template: __webpack_require__(/*! ./conformation.component.html */ "./src/app/component/conformation/conformation.component.html"),
            styles: [__webpack_require__(/*! ./conformation.component.css */ "./src/app/component/conformation/conformation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_appstate_service__WEBPACK_IMPORTED_MODULE_2__["AppstateService"]])
    ], ConformationComponent);
    return ConformationComponent;
}());



/***/ }),

/***/ "./src/app/component/customers/customers.component.css":
/*!*************************************************************!*\
  !*** ./src/app/component/customers/customers.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog {\r\n  height: 100%;\r\n  width: 100%;\r\n  background-color: #46464657;\r\n  position: fixed;\r\n  top: 0;\r\n  z-index: 9999;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2N1c3RvbWVycy9jdXN0b21lcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsMkJBQTJCO0VBQzNCLGVBQWU7RUFDZixNQUFNO0VBQ04sYUFBYTtBQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2N1c3RvbWVycy9jdXN0b21lcnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kaWFsb2cge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDY0NjQ2NTc7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogMDtcclxuICB6LWluZGV4OiA5OTk5O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/component/customers/customers.component.html":
/*!**************************************************************!*\
  !*** ./src/app/component/customers/customers.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"container-fluid\" style=\"margin-top: 20px\">\n    <div class=\"form-group\" [hidden]=\"!this.initForm.firstName.show\" >\n      <label for=\"firstname\" >First Name <span style=\"color: red\"> {{this.initForm.firstName.required ? '*': ''}} </span> </label>\n       <input id=\"firstname\" type=\"text\" name=\"firstName\" class=\"form-control\" [(ngModel)]=\"customerModel.firstName\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.lastName.show\">\n      <label for=\"lastname\">Last Name <span style=\"color: red\"> {{this.initForm.lastName.required ? '*': ''}} </span></label>\n      <input id=\"lastname\" type=\"text\" name=\"lastName\" class=\"form-control\" [(ngModel)]=\"customerModel.lastName\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.dateOfBirth.show\">\n      <label for=\"birthdate\">Birthdate <span style=\"color: red\"> {{this.initForm.dateOfBirth.required ? '*': ''}} </span></label>\n      <input id=\"birthdate\" type=\"date\" name=\"dateOfBirth\" class=\"form-control\" [(ngModel)]=\"customerModel.dateOfBirth\" >\n    </div>\n    <div class=\"form-group\">\n      <div [hidden]=\"!this.initForm.identificationNumber.show\">\n        <label for=\"identification\" >Identification <span style=\"color: red\"> {{this.initForm.identificationNumber.required ? '*': ''}} </span></label>\n        <div class=\"input-group\">\n          <select *ngIf=\"this.initForm.identificationNumber.options.length > 0\" class=\"form-control col-3 bg-light\" [(ngModel)]=\"customerModel.identificationNumber.index\">\n              <option *ngFor=\"let identification of this.initForm.identificationNumber.options\" [value]=\"identification.index\"> {{identification.title}}</option>\n          </select>\n        <input id=\"identification\" type=\"text\" name=\"identificationNumber\" class=\"form-control col-9\" [(ngModel)]=\"customerModel.identificationNumber.value\">\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.email.show\">\n      <label for=\"email\">Email <span style=\"color: red\"> {{this.initForm.email.required ? '*': ''}} </span></label>\n      <input id=\"email\" type=\"email\" name=\"email\" class=\"form-control\"  [(ngModel)]=\"customerModel.email\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.phone.show\">\n      <label for=\"phone\">Phone <span style=\"color: red\"> {{this.initForm.phone.required ? '*': ''}} </span></label>\n      <input id=\"phone\" type=\"number\" name=\"phone\" class=\"form-control\" [(ngModel)]=\"customerModel.phone\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.notes.show\">\n      <label for=\"notes\">Notes </label>\n      <input id=\"notes\" type=\"text\" name=\"notes\" class=\"form-control\" [(ngModel)]=\"customerModel.notes\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.addressState.show\">\n      <label for=\"addressState\">State <span style=\"color: red\"> {{this.initForm.addressState.required ? '*': ''}} </span></label>\n      <input id=\"addressState\" type=\"text\" name=\"addressState\" class=\"form-control\" [(ngModel)]=\"customerModel.addressState\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.addressLine1.show\">\n      <label for=\"notes\">Address <span style=\"color: red\"> {{this.initForm.addressLine1.required ? '*': ''}} </span></label>\n      <input id=\"addressLine1\" type=\"text\" name=\"addressLine1\" class=\"form-control\" [(ngModel)]=\"customerModel.addressLine1\">\n      <input id=\"addressLine2\" type=\"text\" name=\"addressLine2\" class=\"form-control\" [(ngModel)]=\"customerModel.addressLine2\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.addressCity.show\">\n      <label for=\"addressCity\">City <span style=\"color: red\"> {{this.initForm.addressCity.required ? '*': ''}} </span></label>\n      <input id=\"addressCity\" type=\"text\" name=\"addressCity\" class=\"form-control\" [(ngModel)]=\"customerModel.addressCity\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.addressZip.show\">\n      <label for=\"addressZip\">Zip code <span style=\"color: red\"> {{this.initForm.addressZip.required ? '*': ''}} </span></label>\n      <input id=\"addressZip\" type=\"text\" name=\"addressZip\" class=\"form-control\" [(ngModel)]=\"customerModel.addressZip\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.addressCountry.show\">\n      <label for=\"addressCountry\">Country <span style=\"color: red\"> {{this.initForm.addressCountry.required ? '*': ''}} </span> </label>\n      <input id=\"addressCountry\" type=\"text\" name=\"addressCountry\" class=\"form-control\" [(ngModel)]=\"customerModel.addressCountry\">\n    </div>\n    <div class=\"form-group\" [hidden]=\"!this.initForm.cardNumber.show\">\n      <label for=\"cardNumber\">CardNumber <span style=\"color: red\"> {{this.initForm.cardNumber.required ? '*': ''}} </span> </label>\n      <input id=\"cardNumber\" type=\"text\" name=\"cardNumber\" class=\"form-control\" [(ngModel)]=\"customerModel.cardNumber\">\n    </div>\n    <div class=\"form-group\">\n      <div>\n        <label for=\"terms\" >Terms</label>\n        <div *ngFor=\"let term of this.initForm.terms; index as i\" [hidden]=\"!term.show\">\n          <span><input  id=\"terms\" type=\"checkbox\" name=\"txtTerm{{i}}\" >  </span> <span style=\"color: red\"> {{term.required ? '*': ''}} </span>\n           {{term.content}} <a href=\"{{term.link.url}}\" target=\"_blank\" > {{term.link.title}}</a>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"container-fluid align-items-center\" style=\"margin-top: 20px\">\n    <button class=\"btn btn-success btn-block\" (click)=\"onNext()\" [disabled]=\"!isFormValid\">Next</button>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/component/customers/customers.component.ts":
/*!************************************************************!*\
  !*** ./src/app/component/customers/customers.component.ts ***!
  \************************************************************/
/*! exports provided: CustomersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersComponent", function() { return CustomersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/appstate.service */ "./src/app/services/appstate.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ui-configration-service.service */ "./src/app/services/ui-configration-service.service.ts");
/* harmony import */ var _services_calendar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/calendar.service */ "./src/app/services/calendar.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var CustomersComponent = /** @class */ (function () {
    function CustomersComponent(appStateService, uiConfigrationService, calendarService, router) {
        var _this = this;
        this.appStateService = appStateService;
        this.uiConfigrationService = uiConfigrationService;
        this.calendarService = calendarService;
        this.router = router;
        this.branchState = {};
        this.customerModel = {
            firstName: '',
            lastName: '',
            dateOfBirth: '',
            identificationNumber: { index: 0, value: '' },
            email: '',
            phone: '',
            notes: '',
            addressState: '',
            addressLine1: '',
            addressLine2: '',
            addressCity: '',
            addressZip: '',
            addressCountry: '',
            cardNumber: ''
        };
        this.isFormValid = false;
        this.branchStateObserver = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subscription"]();
        this.branchState = this.appStateService.getBranchState();
        this.initForm = this.uiConfigrationService.getUiConfigration();
        console.log(this.initForm);
        Object.keys(this.initForm).forEach(function (element) {
            if (_this.initForm[element].required)
                _this.initForm[element].isValid = false;
            else
                _this.initForm[element].isValid = true;
        });
        this.reservationState = this.appStateService.getAppointmentReservation();
        this.branchStateObserver = this.appStateService.branchStateObserver.subscribe(function (branch) {
            _this.branchState = branch;
        });
        document.addEventListener('change', function (ele) {
            _this.onChange(ele);
        });
    }
    CustomersComponent.prototype.ngOnInit = function () {
    };
    CustomersComponent.prototype.onChange = function (element) {
        console.log(element);
        console.log(this.initForm);
        if (element.target.type === 'text' || element.target.type === 'date') {
            if (element.target.value) {
                this.initForm[element.target.name].isValid = true;
            }
            else {
                this.initForm[element.target.name].isValid = false;
            }
        }
        this.checkFormValidation();
    };
    CustomersComponent.prototype.checkFormValidation = function () {
        var _this = this;
        this.isFormValid = true;
        Object.keys(this.initForm).forEach(function (ele) {
            if (_this.initForm[ele].isValid === false) {
                _this.isFormValid = false;
                return;
            }
        });
    };
    CustomersComponent.prototype.ngOnDestroy = function () {
        this.branchStateObserver.unsubscribe();
    };
    CustomersComponent.prototype.onNext = function () {
        this.appStateService.setCustomerStateObserver(this.customerModel);
        this.router.navigate(['/verification']);
    };
    CustomersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customers',
            template: __webpack_require__(/*! ./customers.component.html */ "./src/app/component/customers/customers.component.html"),
            styles: [__webpack_require__(/*! ./customers.component.css */ "./src/app/component/customers/customers.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_appstate_service__WEBPACK_IMPORTED_MODULE_2__["AppstateService"],
            _services_ui_configration_service_service__WEBPACK_IMPORTED_MODULE_4__["UiConfigrationServiceService"],
            _services_calendar_service__WEBPACK_IMPORTED_MODULE_5__["CalendarService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], CustomersComponent);
    return CustomersComponent;
}());



/***/ }),

/***/ "./src/app/component/verification/verification.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/component/verification/verification.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC92ZXJpZmljYXRpb24vdmVyaWZpY2F0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/component/verification/verification.component.html":
/*!********************************************************************!*\
  !*** ./src/app/component/verification/verification.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"container-fluid\" style=\"margin-top: 20px\">\n    <div>\n      <div class=\"border border-info rounded\">\n        <div class=\"bg-info p-1 text-white font-weight-bold text-center\">\n          Branch\n        </div>\n      <div class=\"p-1\">\n        <span class=\"font-weight-bold\">   Warsaw </span>\n        <br>\n        <span class=\"font-weight-light\">\n          {{branchState.branch.addressLine1}} {{branchState.branch.addressLine2}}\n          <br>\n          {{branchState.branch.addressCity}} {{branchState.branch.addressCountry}} {{branchState.branch.addressZip}}\n        </span>\n      </div>\n      </div>\n\n      <div class=\"border border-info rounded\">\n        <div class=\"bg-info p-1 text-white font-weight-bold text-center\">\n          Service\n        </div>\n        <div class=\"p-1\">\n          <span class=\"font-weight-bold\">   {{branchState.service.name}} </span>\n        </div>\n      </div>\n\n      <div class=\"border border-info rounded\">\n        <div class=\"bg-info p-1 text-white font-weight-bold text-center\">\n          Time & Date\n        </div>\n        <div class=\"p-1\">\n          <span class=\"font-weight-bold\">  {{branchState.date | date:'dd-MM-yyyy'}}</span>\n          <span class=\"font-weight-bold\">  {{branchState.time}}</span>\n        </div>\n      </div>\n\n      <div class=\"border border-info rounded\">\n        <div class=\"bg-info p-1 text-white font-weight-bold text-center\">\n          Personal Information\n        </div>\n        <div class=\"p-1 \">\n          <span class=\"font-weight-bold\"> {{personalState | json}} </span>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container-fluid align-items-center\" style=\"margin-top: 20px\">\n    <button class=\"btn btn-success btn-block\" (click)=\"confirmAppointment()\">Next</button>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/component/verification/verification.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/component/verification/verification.component.ts ***!
  \******************************************************************/
/*! exports provided: VerificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificationComponent", function() { return VerificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/appstate.service */ "./src/app/services/appstate.service.ts");
/* harmony import */ var _services_calendar_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/calendar.service */ "./src/app/services/calendar.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var VerificationComponent = /** @class */ (function () {
    function VerificationComponent(appStateService, calendarService, route) {
        this.appStateService = appStateService;
        this.calendarService = calendarService;
        this.route = route;
        this.branchState = this.appStateService.getBranchState();
        this.reservationState = this.appStateService.getAppointmentReservation();
        this.personalState = this.appStateService.getCustomerState();
        console.log(this.personalState);
        console.log(this.reservationState);
    }
    VerificationComponent.prototype.ngOnInit = function () {
    };
    VerificationComponent.prototype.confirmAppointment = function () {
        var _this = this;
        // let temp = {
        //   firstName : this.customerModel.firstName,
        //   lastName : this.customerModel.lastname,
        //   identificationNumber: this.customerModel.identification.value
        // };
        //
        // console.log(temp);
        //
        this.calendarService.confirmAppointment(this.reservationState.publicId, this.personalState).then(function (data) {
            _this.appStateService.setConfirmationStateObserver(data);
            console.log('setConfirmationStateObserver', data);
            _this.route.navigateByUrl("/confirmation");
        });
    };
    VerificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-verification',
            template: __webpack_require__(/*! ./verification.component.html */ "./src/app/component/verification/verification.component.html"),
            styles: [__webpack_require__(/*! ./verification.component.css */ "./src/app/component/verification/verification.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_appstate_service__WEBPACK_IMPORTED_MODULE_2__["AppstateService"],
            _services_calendar_service__WEBPACK_IMPORTED_MODULE_3__["CalendarService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], VerificationComponent);
    return VerificationComponent;
}());



/***/ }),

/***/ "./src/app/guards/branch.guard.ts":
/*!****************************************!*\
  !*** ./src/app/guards/branch.guard.ts ***!
  \****************************************/
/*! exports provided: BranchGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchGuard", function() { return BranchGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_appstate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/appstate.service */ "./src/app/services/appstate.service.ts");




var BranchGuard = /** @class */ (function () {
    function BranchGuard(appState, route) {
        this.appState = appState;
        this.route = route;
    }
    BranchGuard.prototype.canActivate = function (route, state) {
        var currentStage = localStorage.getItem('currentStage');
        console.log('currentStage', currentStage);
        if (currentStage === '0') {
            return true;
        }
        return false;
    };
    BranchGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_appstate_service__WEBPACK_IMPORTED_MODULE_3__["AppstateService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], BranchGuard);
    return BranchGuard;
}());



/***/ }),

/***/ "./src/app/guards/confirmation.guard.ts":
/*!**********************************************!*\
  !*** ./src/app/guards/confirmation.guard.ts ***!
  \**********************************************/
/*! exports provided: ConfirmationGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationGuard", function() { return ConfirmationGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ConfirmationGuard = /** @class */ (function () {
    function ConfirmationGuard(router) {
        this.router = router;
    }
    ConfirmationGuard.prototype.canActivate = function (route, state) {
        var currentStage = localStorage.getItem('currentStage');
        console.log('currentStage', currentStage);
        if (currentStage === '3') {
            return true;
        }
        this.router.navigateByUrl('/branches');
        return false;
    };
    ConfirmationGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ConfirmationGuard);
    return ConfirmationGuard;
}());



/***/ }),

/***/ "./src/app/guards/customer.guard.ts":
/*!******************************************!*\
  !*** ./src/app/guards/customer.guard.ts ***!
  \******************************************/
/*! exports provided: CustomerGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerGuard", function() { return CustomerGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var CustomerGuard = /** @class */ (function () {
    function CustomerGuard(router) {
        this.router = router;
    }
    CustomerGuard.prototype.canActivate = function (route, state) {
        var currentStage = localStorage.getItem('currentStage');
        console.log('currentStage', currentStage);
        if (currentStage === '1') {
            return true;
        }
        this.router.navigateByUrl('/branches');
        return false;
    };
    CustomerGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CustomerGuard);
    return CustomerGuard;
}());



/***/ }),

/***/ "./src/app/guards/verification.guard.ts":
/*!**********************************************!*\
  !*** ./src/app/guards/verification.guard.ts ***!
  \**********************************************/
/*! exports provided: VerificationGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificationGuard", function() { return VerificationGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var VerificationGuard = /** @class */ (function () {
    function VerificationGuard(router) {
        this.router = router;
    }
    VerificationGuard.prototype.canActivate = function (route, state) {
        var currentStage = localStorage.getItem('currentStage');
        console.log('currentStage', currentStage);
        if (currentStage === '2') {
            return true;
        }
        this.router.navigateByUrl('/branches');
        return false;
    };
    VerificationGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], VerificationGuard);
    return VerificationGuard;
}());



/***/ }),

/***/ "./src/app/routers/router/router.module.ts":
/*!*************************************************!*\
  !*** ./src/app/routers/router/router.module.ts ***!
  \*************************************************/
/*! exports provided: RoutersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutersModule", function() { return RoutersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _component_branches_branches_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../component/branches/branches.component */ "./src/app/component/branches/branches.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _component_customers_customers_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../component/customers/customers.component */ "./src/app/component/customers/customers.component.ts");
/* harmony import */ var _component_conformation_conformation_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../component/conformation/conformation.component */ "./src/app/component/conformation/conformation.component.ts");
/* harmony import */ var _guards_branch_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../guards/branch.guard */ "./src/app/guards/branch.guard.ts");
/* harmony import */ var _guards_customer_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../guards/customer.guard */ "./src/app/guards/customer.guard.ts");
/* harmony import */ var _component_verification_verification_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../component/verification/verification.component */ "./src/app/component/verification/verification.component.ts");
/* harmony import */ var _guards_verification_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../guards/verification.guard */ "./src/app/guards/verification.guard.ts");
/* harmony import */ var _guards_confirmation_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../guards/confirmation.guard */ "./src/app/guards/confirmation.guard.ts");











var appRoutes = [
    { path: '', redirectTo: '/branches', pathMatch: 'full' },
    { path: 'branches', component: _component_branches_branches_component__WEBPACK_IMPORTED_MODULE_2__["BranchesComponent"], canActivate: [_guards_branch_guard__WEBPACK_IMPORTED_MODULE_6__["BranchGuard"]] },
    { path: 'customer', component: _component_customers_customers_component__WEBPACK_IMPORTED_MODULE_4__["CustomersComponent"], canActivate: [_guards_customer_guard__WEBPACK_IMPORTED_MODULE_7__["CustomerGuard"]] },
    { path: 'verification', component: _component_verification_verification_component__WEBPACK_IMPORTED_MODULE_8__["VerificationComponent"], canActivate: [_guards_verification_guard__WEBPACK_IMPORTED_MODULE_9__["VerificationGuard"]] },
    { path: 'confirmation', component: _component_conformation_conformation_component__WEBPACK_IMPORTED_MODULE_5__["ConformationComponent"], canActivate: [_guards_confirmation_guard__WEBPACK_IMPORTED_MODULE_10__["ConfirmationGuard"]] }
];
var RoutersModule = /** @class */ (function () {
    function RoutersModule() {
    }
    RoutersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes, { enableTracing: false } // <-- debugging purposes only
                )
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
        })
    ], RoutersModule);
    return RoutersModule;
}());



/***/ }),

/***/ "./src/app/services/appstate.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/appstate.service.ts ***!
  \**********************************************/
/*! exports provided: AppstateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppstateService", function() { return AppstateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppstateService = /** @class */ (function () {
    function AppstateService() {
        this.ApplicationStages = {
            0: { id: 0, title: 'Init Application', nextUrl: '/branches' },
            1: { id: 1, title: 'branch complete', },
            2: { id: 2, title: 'reservation complete', nextUrl: '/customer' },
            3: { id: 3, title: 'customer complete', nextUrl: '/verfication' },
            4: { id: 4, title: 'information verified', nextUrl: '/confirmation' },
            5: { id: 5, title: 'confirm appointment' }
        };
        this.currentState = 0;
        this.applicationStagesObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.branchState = {};
        this.appointmentReservation = {};
        this.customerState = {};
        this.confirmationState = {};
        this.branchStateObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.appointmentReservationObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.customerStateObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.verificationStateObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.confirmationStateObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.finishedBookingObserver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        localStorage.setItem('currentStage', String(this.currentState));
    }
    AppstateService.prototype.getBranchState = function () {
        return this.branchState;
    };
    AppstateService.prototype.getCustomerState = function () {
        return this.customerState;
    };
    AppstateService.prototype.getAppointmentReservation = function () {
        return this.appointmentReservation;
    };
    AppstateService.prototype.getConfirmationState = function () {
        return this.confirmationState;
    };
    AppstateService.prototype.setBranchState = function (branchState) {
        this.branchState = branchState;
        this.branchStateObserver.next(branchState);
    };
    AppstateService.prototype.setAppointmentReservationObserver = function (appointmentReservation) {
        this.appointmentReservation = appointmentReservation;
        this.appointmentReservationObserver.next(appointmentReservation);
        this.updateApplicationStage();
    };
    AppstateService.prototype.setCustomerStateObserver = function (customerState) {
        this.customerState = customerState;
        this.customerStateObserver.next(customerState);
        this.updateApplicationStage();
    };
    AppstateService.prototype.setVerificationStateObserver = function (verificationState) {
        this.verificationStateObserver.next(verificationState);
        this.updateApplicationStage();
    };
    AppstateService.prototype.setConfirmationStateObserver = function (confirmationState) {
        this.confirmationState = confirmationState;
        this.confirmationStateObserver.next(confirmationState);
        this.updateApplicationStage();
    };
    AppstateService.prototype.setFinishedBookingObserver = function () {
        this.finishedBookingObserver.next('done');
    };
    AppstateService.prototype.updateApplicationStage = function () {
        this.currentState++;
        localStorage.setItem('currentStage', String(this.currentState));
        this.publishApplicationStage();
    };
    AppstateService.prototype.getCurrentStage = function () {
        return this.ApplicationStages[this.currentState];
    };
    AppstateService.prototype.publishApplicationStage = function () {
        this.applicationStagesObserver.emit(this.ApplicationStages[this.currentState]);
    };
    AppstateService.prototype.resetAllState = function () {
        this.branchState = {};
        this.appointmentReservation = {};
        this.customerState = {};
        this.confirmationState = {};
        this.currentState = 0;
        localStorage.setItem('currentStage', "0");
        this.publishApplicationStage();
    };
    AppstateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppstateService);
    return AppstateService;
}());



/***/ }),

/***/ "./src/app/services/calendar.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/calendar.service.ts ***!
  \**********************************************/
/*! exports provided: CalendarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarService", function() { return CalendarService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CalendarService = /** @class */ (function () {
    function CalendarService(httpClient) {
        this.httpClient = httpClient;
    }
    CalendarService.prototype.getBranches = function () {
        return this.httpClient.get('/calendar/branches').toPromise();
    };
    CalendarService.prototype.getServicesByBranchPublicId = function (selectedBrachPublicId) {
        return this.httpClient.get('/calendar/branches/' + selectedBrachPublicId + '/services').toPromise();
    };
    CalendarService.prototype.getDatesByBranchPublicIdAndServicePublicId = function (selectedBrachPublicId, selectedServicePublicId) {
        return this.httpClient.get('/calendar/branches/' + selectedBrachPublicId + '/services/' + selectedServicePublicId + '/dates/').toPromise();
    };
    CalendarService.prototype.getTimeslotsByBranchPublicIdAndServicePublicIdAndDate = function (selectedBrachPublicId, selectedServicePublicId, date) {
        return this.httpClient.get('/calendar/branches/' + selectedBrachPublicId + '/services/' + selectedServicePublicId + '/dates/' + date + '/timeslots/').toPromise();
    };
    CalendarService.prototype.reserveAppointment = function (branchInfo) {
        var tempSelectedDate = new Date(branchInfo.date);
        var yyyyMMddString = tempSelectedDate.getFullYear() + '-' + (tempSelectedDate.getMonth() + 1) + '-' + tempSelectedDate.getDate();
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.set('content-Type', 'application/json');
        return this.httpClient.post('/calendar/appointment/reserve', { branchPublicId: branchInfo.branch.publicId, servicePublicId: branchInfo.service.publicId, date: yyyyMMddString, time: branchInfo.time }, { headers: headers }).toPromise();
    };
    CalendarService.prototype.confirmAppointment = function (reservationPublicId, customer) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.set('content-Type', 'application/json');
        var tempCustomer = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, customer);
        delete tempCustomer.notes;
        delete tempCustomer.identificationNumber;
        tempCustomer.identificationNumber = customer.identificationNumber.value;
        var request = { notes: customer.notes, custome: '', title: '', customer: tempCustomer };
        console.log('confirmAppointment', request);
        return this.httpClient.post('/calendar/appointment/confirm?appointmentPublicId=' + reservationPublicId, request, { headers: headers }).toPromise();
    };
    CalendarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CalendarService);
    return CalendarService;
}());



/***/ }),

/***/ "./src/app/services/ui-configration-service.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/ui-configration-service.service.ts ***!
  \*************************************************************/
/*! exports provided: UiConfigrationServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiConfigrationServiceService", function() { return UiConfigrationServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var UiConfigrationServiceService = /** @class */ (function () {
    function UiConfigrationServiceService(http) {
        this.http = http;
    }
    UiConfigrationServiceService_1 = UiConfigrationServiceService;
    UiConfigrationServiceService.prototype.fetchUiConfigration = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, _b;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.http.get("/configration/ui").toPromise()];
                    case 1:
                        _a.uiConfig = _c.sent();
                        console.log("Browser Language : ", navigator.language);
                        _b = UiConfigrationServiceService_1;
                        return [4 /*yield*/, this.http.get("assets/language/messages_" + navigator.language + ".json").toPromise()];
                    case 2:
                        _b.myLanguage = _c.sent();
                        return [2 /*return*/, this.uiConfig];
                }
            });
        });
    };
    UiConfigrationServiceService.prototype.getUiConfigration = function () {
        return this.uiConfig;
    };
    var UiConfigrationServiceService_1;
    UiConfigrationServiceService = UiConfigrationServiceService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UiConfigrationServiceService);
    return UiConfigrationServiceService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! J:\iFlow-iq-calendar\calendar-client-frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map