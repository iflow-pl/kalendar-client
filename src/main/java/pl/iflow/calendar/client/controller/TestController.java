package pl.iflow.calendar.client.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.iflow.calendar.client.manager.PropertyManager;
import pl.iflow.calendar.client.service.CalendarProxyService;

@RestController
@RequestMapping("/test")
public class TestController {

	@Autowired
	PropertyManager propertyManager;
	
	@Autowired
	CalendarProxyService calendarProxyService;
	
	@GetMapping("/ping")
	public String getPing() {
		return "Ping...";
	}
	
	
	@GetMapping("/readProperties")
	public String getReadProperties() {
		try {
		System.out.println("Application Name : " + propertyManager.getApplicationName());
		System.out.println("Use Api Gateway : " + (propertyManager.getUseApiGateway()? "Yes" : "No"));
		System.out.println("Use Api Gateway URL : " + propertyManager.getApiGatewayUrl());
		}
		catch (Exception e) {
			return "Fail : " + e.getMessage();
		}
		
		return "Success";
	}
	
	
}
