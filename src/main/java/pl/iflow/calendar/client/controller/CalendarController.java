package pl.iflow.calendar.client.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.qp.api.connectors.dto.BranchListBranch;
import com.qmatic.qp.api.customer.Customer;
import com.qmatic.qp.calendar.model.Branch;

import pl.iflow.calendar.client.manager.PropertyManager;
import pl.iflow.calendar.client.model.ConfirmationRequest; 
import pl.iflow.calendar.client.model.ReservationRequest;
import pl.iflow.calendar.client.service.CalendarProxyService; 

@RestController
@RequestMapping("/calendar")
public class CalendarController {

	@Autowired
	PropertyManager propertyManager;
	
	@Autowired
	CalendarProxyService calendarProxyService;
	
	
	
	@GetMapping("/branches")
	public List<Branch>  getBranches() {
		
		  
		  
		 return  (List<Branch>) this.calendarProxyService.getAvailableBranches().get("branchList");  
		 
	}
	
	@GetMapping("/branches/{branchPublicId}/services")
	public Object getServices(@PathVariable("branchPublicId") String branchPublicId) {
		 
		 return this.calendarProxyService.getServiceGroups(branchPublicId).get("serviceList");
	 
	}
	
	@GetMapping("/branches/{branchPublicId}/services/{servicePublicId}/dates")
	public Object getDates(
			@PathVariable("branchPublicId") String branchPublicId,
			@PathVariable("servicePublicId") String servicePublicId) {
		 
		return this.calendarProxyService.getAvailableDates(branchPublicId, servicePublicId).get("dates");
		 
	}
	
	@GetMapping("/branches/{branchPublicId}/services/{servicePublicId}/dates/{date}/timeslots")
	public Object getTimeSlots(
			@PathVariable("branchPublicId") String branchPublicId,
			@PathVariable("servicePublicId") String servicePublicId,
			@PathVariable("date") String date) {
		
			return this.calendarProxyService.getAvailableTimes(branchPublicId, servicePublicId, date).get("times");
	
	}
	
	@PostMapping("/appointment/reserve")
	public Map<String, Object> reserveAppointment(
			@RequestBody ReservationRequest reservationRequest) {
		
		try {
						
			return this.calendarProxyService.reserveAppointmentOfSingleService(reservationRequest.getBranchPublicId(), reservationRequest.getDate(), reservationRequest.getTime(), reservationRequest.getServicePublicId());
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			return null;
		}		
	}

	@PostMapping("/appointment/confirm")
	public Map<String, Object> confirmAppointment(@RequestParam("appointmentPublicId") String appointmentPublicId,
									 @RequestBody ConfirmationRequest confirmationRequest) {
		
	 
		
		try {
			
			System.out.println("appointmentPublicId : " + appointmentPublicId);
			System.out.println("getIdentificationNumber : " + confirmationRequest.getCustomer().getIdentificationNumber());
			
			return this.calendarProxyService.confirmAppointment(appointmentPublicId, confirmationRequest);
			
		} catch(Exception e) {
			
			System.out.println(e.getMessage());
			return null;
		} 
	}
	
	
	@PostMapping("/customer")
	public String createCustomerObject() {
		try {			
			
			this.calendarProxyService.createNewCustomer("");
			
		} catch(Exception e) {
			return "Fail : " + e.getMessage();
		}
		return "Success";
	}
	
}
