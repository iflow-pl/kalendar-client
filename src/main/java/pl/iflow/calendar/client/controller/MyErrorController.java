package pl.iflow.calendar.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import pl.iflow.calendar.client.manager.PropertyManager; 

@Controller
public class MyErrorController implements ErrorController {

	@Autowired
	PropertyManager propertyManager; 
	
   @RequestMapping("/error")
    public ModelAndView handle(Exception ex) { 

    	ModelAndView mv = new ModelAndView();
        mv.addObject("message", ex.getMessage());
        
        StringBuilder sb = new StringBuilder("redirect:");
        sb.append(this.propertyManager.getRedirectOnError());
        mv.setViewName(sb.toString());

        return mv;
    }

@Override
public String getErrorPath() {
	// TODO Auto-generated method stub
	return null;
}
 
}
