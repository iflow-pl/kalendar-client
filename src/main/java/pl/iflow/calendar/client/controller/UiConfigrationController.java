package pl.iflow.calendar.client.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.iflow.calendar.client.manager.PropertyManager;

@RestController
@RequestMapping("/configration")
public class UiConfigrationController {
	
	@Autowired
	PropertyManager propertyManager;
	
	@GetMapping("/ui")
	public JsonNode getUiConfirtation() {
			 		
		String config = this.propertyManager.getUiConfig();
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode in = null;
		try {
			in = objectMapper.readTree(config);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return in;
	}
}
