package pl.iflow.calendar.client.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@Controller
public class EntryPointController{
	
	@PostMapping("/appointment")
	public String startWith(
			@Nullable @RequestParam("bidfe") String branchPublicId,
			@Nullable @RequestParam("sidfe") String servicePublicId,
			@Nullable @RequestParam("btfe") String date,
			@Nullable @RequestParam("tsfe") String timeslot,
			Model model ) {
		 
		
		
		if(branchPublicId != null) {
			
			model.addAttribute("branchPublicId", branchPublicId ); 
			
			if(servicePublicId != null) {
				
				model.addAttribute("servicePublicId", servicePublicId );
				
				if(date != null) {
					
					model.addAttribute("date", date );
					
					if(timeslot != null) {						 
						
						model.addAttribute("timeslot", timeslot );
						
					}
					 
				}			 
			} 		
		} 
				
		
		return "index";
	} 
}
