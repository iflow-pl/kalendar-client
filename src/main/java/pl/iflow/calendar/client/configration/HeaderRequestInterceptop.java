package pl.iflow.calendar.client.configration;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class HeaderRequestInterceptop implements ClientHttpRequestInterceptor{

	private String key = "";
	private String value = "";
	
	public HeaderRequestInterceptop() { 
		
	}
	
	public HeaderRequestInterceptop(String key, String value) {
		this.key = key;
		this.value = value;				
	}
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		request.getHeaders().add(this.key, this.value);
		return execution.execute(request, body);
		
	}

}
