package pl.iflow.calendar.client.configration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

import pl.iflow.calendar.client.manager.PropertyManager;
import pl.iflow.calendar.client.service.CalendarProxyService;
import pl.iflow.calendar.client.service.impl.GatewayChannelServiceImpl;
import pl.iflow.calendar.client.service.impl.OrchestraChannelServiceImpl;

@Configuration
public class ApplicationConfig {

	@Autowired
	PropertyManager propertyManager;
	
	@Bean
	public RestTemplate getRestTemplate() {
		
		RestTemplate restTemplate = new RestTemplate();
		
		if(propertyManager.getUseApiGateway()) { 	
			System.out.println("Using Gateway Connector");
			restTemplate.getInterceptors().add(new HeaderRequestInterceptop("auth-Token", propertyManager.getAuthToken()));
		} else {
			System.out.println("Using Orchestra Connector");
			restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(propertyManager.getOrchestraUsername(), propertyManager.getOrchestraPassword()));						
		}
		
		return restTemplate;
	}
	
	@Bean
	public CalendarProxyService getCalendarProxyService() {
		if(propertyManager.getUseApiGateway()) { 
			System.out.println("Using GatewayChannelServiceImpl");
			return new GatewayChannelServiceImpl();
		} else {
			System.out.println("Using OrchestraChannelServiceImpl");
			return new OrchestraChannelServiceImpl();
		}
	}
	
	
}
