package pl.iflow.calendar.client.model;

public class ReservationRequest {

	private String branchPublicId;
	private String servicePublicId;
	private String date;
	private String time;
	
	public String getBranchPublicId() {
		return branchPublicId;
	}
	public void setBranchPublicId(String branchPublicId) {
		this.branchPublicId = branchPublicId;
	}
	public String getServicePublicId() {
		return servicePublicId;
	}
	public void setServicePublicId(String servicePublicId) {
		this.servicePublicId = servicePublicId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
  
	
}
