package pl.iflow.calendar.client.service;

import java.util.Map;

public interface CalendarProxyService {

	Map<String, Object> getAvailableBranches();
	Map<String, Object> getServiceGroups(String branchPublicId);
	Map<String, Object> getAvailableDates(String branchPublicId, String servicePublicId);
	Map<String, Object> getAvailableTimes(String branchPublicId, String servicePublicId, String date);
	Map<String, Object> reserveAppointmentOfSingleService(String branchPublicId, String date, String time, String servicePublicIds);
	
	Map<String, Object> createNewCustomer(Object customer);
	Map<String, Object> confirmAppointment(String appointmentPublicId, Object customer);
	
}
