package pl.iflow.calendar.client.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import pl.iflow.calendar.client.manager.PropertyManager;
import pl.iflow.calendar.client.manager.VariableManager;
import pl.iflow.calendar.client.service.CalendarProxyService;

public class GatewayChannelServiceImpl implements CalendarProxyService {

	@Autowired
	PropertyManager propertyManager;
	
	@Autowired
	RestTemplate restTemplate;	
		 
	
	public Map<String, Object>  getAvailableBranches() {
		
		Map<String, Object> responseAvailableBranch = this.restTemplate.getForObject(propertyManager.getApiGatewayUrl() + VariableManager.availableBranchPathAG, Map.class);
		return responseAvailableBranch;
	}
	
	public Map<String, Object> getServiceGroups(String branchPublicId) {
		
		String tempServices = VariableManager.availableServicesPathAG.replace("BRANCH_PUBLIC_ID", branchPublicId);
		
		Map<String, Object> responseServices = this.restTemplate.getForObject(propertyManager.getApiGatewayUrl() + tempServices, Map.class);
		
		return responseServices;
	}
	
	public Map<String, Object> getAvailableDates(String branchPublicId, String servicePublicId) {
	  		
		String tempDates = VariableManager.availableDatesPathAG.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempDates = tempDates.replace("SERVICE_PUBLIC_ID", servicePublicId);		
		
		Map<String, Object> responseDates = this.restTemplate.getForObject(propertyManager.getApiGatewayUrl() + tempDates, Map.class);
		
		return responseDates;
	} 
	
	public Map<String, Object> getAvailableTimes(String branchPublicId, String servicePublicId, String date) {
	 
		String tempTimes = VariableManager.availableTimeslotsPathAG.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempTimes = tempTimes.replace("DATE", date);
		tempTimes = tempTimes.replace("SERVICE_PUBLIC_ID", servicePublicId);		
		
		Map<String, Object> responseTimeslots = this.restTemplate.getForObject(propertyManager.getApiGatewayUrl() + tempTimes,  Map.class);
		
		return responseTimeslots;
	} 
	
	public Map<String, Object> reserveAppointmentOfSingleService(String branchPublicId, String date, String time, String servicePublicIds) {
		
		String tempReserveTimes = VariableManager.reserveTimeslotsPathAG.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempReserveTimes = tempReserveTimes.replace("SERVICE_PUBLIC_ID", servicePublicIds);
		tempReserveTimes = tempReserveTimes.replace("DATE", date);
		tempReserveTimes = tempReserveTimes.replace("TIME", time);		
	 
		Map<String, Object> responseReserveTimeslots = this.restTemplate.postForObject(propertyManager.getApiGatewayUrl() + tempReserveTimes, null, Map.class);
		
		return responseReserveTimeslots;
		
	}	
	
	public Map<String, Object> createNewCustomer(Object customer) {
		 	 
		Map<String, Object> responseCreateCustomer = this.restTemplate.postForObject(propertyManager.getApiGatewayUrl() + VariableManager.createNewCustormAG, customer, Map.class);
 
		return responseCreateCustomer;
		
	}	 
	
	public Map<String, Object> confirmAppointment(String appointmentPublicId, Object customer) {
		 
				 
		String tempConfirmationPath = VariableManager.confirmAppointmentPathAG.replace("APPOINTMENT_PUBLIC_ID", appointmentPublicId);
		
		Map<String, Object> responseConfirmation = this.restTemplate.postForObject(propertyManager.getApiGatewayUrl() + tempConfirmationPath, customer, Map.class);
		
		return responseConfirmation;
		
	}

}
