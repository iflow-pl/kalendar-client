package pl.iflow.calendar.client.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.qp.api.connectors.dto.BranchListBranch;

import pl.iflow.calendar.client.configration.HeaderRequestInterceptop;
import pl.iflow.calendar.client.manager.PropertyManager;
import pl.iflow.calendar.client.manager.VariableManager;
import pl.iflow.calendar.client.service.CalendarProxyService;


public class OrchestraChannelServiceImpl implements CalendarProxyService {

	@Autowired
	PropertyManager propertyManager;
	
	@Autowired
	RestTemplate restTemplate;	
		 
	
	public Map<String, Object>  getAvailableBranches() {
		
		Map<String, Object> responseAvailableBranch = this.restTemplate.getForObject(propertyManager.getOrchestraUrl() + VariableManager.availableBranchPathOrc, Map.class);
		return responseAvailableBranch;
	}
	
	public Map<String, Object> getServiceGroups(String branchPublicId) {
		
		String tempServices = VariableManager.availableServicesPathOrc.replace("BRANCH_PUBLIC_ID", branchPublicId);
		
		Map<String, Object> responseServices = this.restTemplate.getForObject(propertyManager.getOrchestraUrl() + tempServices, Map.class);
		
		return responseServices;
	}
	
	public Map<String, Object> getAvailableDates(String branchPublicId, String servicePublicId) {
	  		
		String tempDates = VariableManager.availableDatesPathOrc.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempDates = tempDates.replace("SERVICE_PUBLIC_ID", servicePublicId);		
		
		Map<String, Object> responseDates = this.restTemplate.getForObject(propertyManager.getOrchestraUrl() + tempDates, Map.class);
		
		return responseDates;
	} 
	
	public Map<String, Object> getAvailableTimes(String branchPublicId, String servicePublicId, String date) {
	 
		String tempTimes = VariableManager.availableTimeslotsPathOrc.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempTimes = tempTimes.replace("DATE", date);
		tempTimes = tempTimes.replace("SERVICE_PUBLIC_ID", servicePublicId);		
		
		Map<String, Object> responseTimeslots = this.restTemplate.getForObject(propertyManager.getOrchestraUrl() + tempTimes,  Map.class);
		
		return responseTimeslots;
	} 
	
	public Map<String, Object> reserveAppointmentOfSingleService(String branchPublicId, String date, String time, String servicePublicIds) {
		
		String tempReserveTimes = VariableManager.reserveTimeslotsPathOrc.replace("BRANCH_PUBLIC_ID", branchPublicId);
		tempReserveTimes = tempReserveTimes.replace("SERVICE_PUBLIC_ID", servicePublicIds);
		tempReserveTimes = tempReserveTimes.replace("DATE", date);
		tempReserveTimes = tempReserveTimes.replace("TIME", time);		
	 
				
		Map<String, Object> responseReserveTimeslots = this.restTemplate.postForObject(propertyManager.getOrchestraUrl() + tempReserveTimes, null, Map.class);
		
		return responseReserveTimeslots;
		
	}	
	
	public Map<String, Object> createNewCustomer(Object customer) {
		 	 
		Map<String, Object> responseCreateCustomer = this.restTemplate.postForObject(propertyManager.getOrchestraUrl() + VariableManager.createNewCustormOrc, customer, Map.class);
 
		return responseCreateCustomer;
		
	}	 
	
	public Map<String, Object> confirmAppointment(String appointmentPublicId, Object customer) {
		 
				 
		String tempConfirmationPath = VariableManager.confirmAppointmentPathOrc.replace("APPOINTMENT_PUBLIC_ID", appointmentPublicId);
		
		Map<String, Object> responseConfirmation = this.restTemplate.postForObject(propertyManager.getOrchestraUrl() + tempConfirmationPath, customer, Map.class);
		
		return responseConfirmation;
		
	}
	
}
