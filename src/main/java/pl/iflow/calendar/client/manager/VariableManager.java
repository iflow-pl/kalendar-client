package pl.iflow.calendar.client.manager;

public class VariableManager {	 
	
	public static String apiPath = "/api";
	
	public static String availableBranchPathOrc = "/calendar-backend/public/api/v1/branches/";
	public static String availableServicesPathOrc = "/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/";  //BRANCH_PUBLIC_ID
	public static String availableDatesPathOrc = "/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/";  //BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID
	public static String availableTimeslotsPathOrc = "/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/DATE/times/"; // BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID,  DATE
	
	public static String reserveTimeslotsPathOrc = "/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/DATE/times/TIME/reserve/"; // BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID , DATE, TIME
	public static String createNewCustormOrc = "/calendar-backend/api/v1/customers/";
	public static String confirmAppointmentPathOrc = "/calendar-backend/public/api/v1/branches/appointments/APPOINTMENT_PUBLIC_ID/confirm/"; // APPOINTMENT_PUBLIC_ID
	
	
	
	
	public static String availableBranchPathAG = "/rest/calendar-backend/public/api/v1/branches/";
	public static String availableServicesPathAG = "/rest/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/";  //BRANCH_PUBLIC_ID
	public static String availableDatesPathAG = "/rest/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/";  //BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID
	public static String availableTimeslotsPathAG = "/rest/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/DATE/times/"; // BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID,  DATE
	
	public static String reserveTimeslotsPathAG = "/rest/calendar-backend/public/api/v1/branches/BRANCH_PUBLIC_ID/services/SERVICE_PUBLIC_ID/dates/DATE/times/TIME/reserve/"; // BRANCH_PUBLIC_ID, SERVICE_PUBLIC_ID , DATE, TIME
	public static String createNewCustormAG = "/rest/calendar-backend/api/v1/customers/";
	public static String confirmAppointmentPathAG = "/rest/calendar-backend/public/api/v1/branches/appointments/APPOINTMENT_PUBLIC_ID/confirm/"; // APPOINTMENT_PUBLIC_ID
	
}
