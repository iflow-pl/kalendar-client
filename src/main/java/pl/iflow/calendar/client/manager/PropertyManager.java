package pl.iflow.calendar.client.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:configuration.properties")
public class PropertyManager {
	
	@Value("${application.name}")
	String applicationName;
	
	@Value("${application.useApiGateway}")
	Boolean useApiGateway;
	
	@Value("${application.apiGatewayUrl}")
	String apiGatewayUrl;
	
	@Value("${application.authToken}")
	String authToken;
	
	@Value("${application.orchestraUrl}")
	String orchestraUrl;
	
	@Value("${application.orchestraUsername}")
	String orchestraUsername;
	
	@Value("${application.orchestraPassword}")
	String orchestraPassword;
	
	@Value("${application.ui}")
	String uiConfig;
	
	@Value("${application.redirectOnError}")
	String redirectOnError;
	
	public String getApplicationName() {
		return applicationName;
	}

	public Boolean getUseApiGateway() {
		return useApiGateway;
	}
	
	public String getApiGatewayUrl() {
		return apiGatewayUrl;
	}
	
	public String getAuthToken() {
		return authToken;
	}
	
	public String getOrchestraUrl() {
		return orchestraUrl;
	}
	
	public String getOrchestraUsername() {
		return orchestraUsername;
	}
	
	public String getOrchestraPassword() {
		return orchestraPassword;
	}
	
	public String getUiConfig() {
		return this.uiConfig;
	}
	
	public String getRedirectOnError() {
		return this.redirectOnError;
	}
	
}
